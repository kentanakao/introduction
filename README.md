# ポートフォリオ

2024.05.17 現在

## プロフィール

Kenta Nakao

### AWS保有資格

全冠保持([Credly](https://www.credly.com/users/kenta-nakao.54adaf61/badges))

![20230828_aws_license](/uploads/67c1e666101635ad27ba5398511f8546/20230828_aws_license.png)

### スキル

- AWS経験  
  - 約2年半
- 開発経験
  - 約5年
    - Python: 3年半
    - Java: 半年

## 実績

### 執筆した技術ブログ記事

- [SageMaker+APIGateway+Lambdaで作るサーバーレスアプリケーション～➀](https://www.cloudbuilders.jp/articles/723/)

- [SageMaker+APIGateway+Lambdaで作るサーバーレスアプリケーション～➁](https://www.cloudbuilders.jp/articles/727/)
   
- [SageMaker+APIGateway+Lambdaで作るサーバーレスアプリケーション～➂(完)](https://www.cloudbuilders.jp/articles/939/)

- [SageMaker+APIGateway+Lambdaで作るサーバーレスアプリケーション～外伝 Cognito認証](https://www.cloudbuilders.jp/articles/2835/)

- [サクッとChaliceでAPI作ってみた！(入門編)](https://www.cloudbuilders.jp/articles/2265/)

- [GitLabリポジトリをCodeCommitにミラーリングしてS3に配置する](https://www.cloudbuilders.jp/articles/1992/)

- [GitLabリポジトリをCodeCommitにミラーリングしてS3に配置する(Vue.js 編)](https://www.cloudbuilders.jp/articles/2006/)

- [GitLabリポジトリをCodeCommitにミラーリングしてCloudFront+S3に配置する(Vue.js+CloudFormation 編)](https://www.cloudbuilders.jp/articles/2665/)

- [AWS IoT Core 初級ハンズオンやってみた(とにかく簡単編)](https://www.cloudbuilders.jp/articles/4109/)

- [AWS構築をCI/CDパイプラインで自動化する(Terraform)](https://www.cloudbuilders.jp/articles/2668/)